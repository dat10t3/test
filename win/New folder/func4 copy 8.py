'''TEST CASE 1 : Write 1 to all bit . read from register with 1 byte '''
# Clear all bit of register
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 4, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
# Set all bit of register
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 4, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
# byte 1
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 1, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
SCHEAP.readByteValue("stm32_f103", 0xFF)
SCHEAP.sc_start(50)
# byte 2
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 1, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
SCHEAP.readByteValue("stm32_f103", 0xFF)
SCHEAP.sc_start(50)
# byte 3
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 1, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
SCHEAP.readByteValue("stm32_f103", 0xFF)
SCHEAP.sc_start(50)
# byte 4
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 1, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
SCHEAP.readByteValue("stm32_f103", 0xFF)
SCHEAP.sc_start(50)


'''TEST CASE 2 : Write 0 to all bit . read from register with 2 byte '''
"TM is PASS"

SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 4, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 4, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
# Frist 2 byte
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 2, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
SCHEAP.readByteValue("stm32_f103", 0xFFFFFFFF)
SCHEAP.sc_start(50)
# Next 2 byte
SCHEAP.issueSend("stm32_f103", 0x01,  CTLREG, 2, 0xFFFFFFFF, 0, 0)
SCHEAP.sc_start(50)
SCHEAP.readByteValue("stm32_f103", 0xFFFFFFFF)
SCHEAP.sc_start(50)
